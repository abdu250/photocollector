//
//  PhotoViewController.swift
//  PhotoCollector
//
//  Created by Abdullah Al-Ahmadi on 1/29/17.
//  Copyright © 2017 Abdullah Al-Ahmadi. All rights reserved.
//

import UIKit

class PhotoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  @IBOutlet weak var imageHolder: UIImageView!
  @IBOutlet weak var imageTitle: UITextField!
  @IBOutlet weak var addToUpdateButton: UIButton!
  
  @IBOutlet weak var deleteButton: UIButton!
  var imagePicker = UIImagePickerController()
  var viewPicker : PhotoEntity? = nil
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    
    imagePicker.delegate = self
    
    if viewPicker == nil {
      print("Add new image") // Go to add new image view
      deleteButton.isHidden = true
    } else{
      // Go to selected image view
      imageHolder.image = UIImage(data: viewPicker?.image as! Data)
      imageTitle.text = viewPicker?.title
      addToUpdateButton.setTitle("Update", for: .normal) // change the "add" button label to "update"
    }
    
    
  }
  
  @IBAction func addPhotoButtonTapped(_ sender: Any) {
    
    if viewPicker == nil { // if add button tapped
      let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
      let photo = PhotoEntity(context: context)
      
      photo.title = imageTitle.text
      photo.image = UIImagePNGRepresentation(imageHolder.image!) as NSData?
      
    } else { // if update button tapped
      viewPicker!.title = imageTitle.text
      viewPicker!.image = UIImagePNGRepresentation(imageHolder.image!) as NSData?
    }
    
    
    
    (UIApplication.shared.delegate as! AppDelegate).saveContext()
    
    navigationController!.popViewController(animated: true) // Dismiss view controller after tapping the add button
    
  }
  @IBAction func cameraButtonTapped(_ sender: Any) {
    imagePicker.sourceType = .camera
    present(imagePicker, animated: true, completion: nil)
  }
  
  @IBAction func photosButtonTapped(_ sender: Any) {
    imagePicker.sourceType = .photoLibrary
    present(imagePicker, animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
    
    imageHolder.image = selectedImage
    imagePicker.dismiss(animated: true, completion: nil)
    
  }
  @IBAction func deleteTapped(_ sender: Any) {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    context.delete(viewPicker!)
    
    (UIApplication.shared.delegate as! AppDelegate).saveContext()
    
    navigationController!.popViewController(animated: true) // Dismiss view controller after tapping the add button
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
}
