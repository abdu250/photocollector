//
//  ViewController.swift
//  PhotoCollector
//
//  Created by Abdullah Al-Ahmadi on 1/29/17.
//  Copyright © 2017 Abdullah Al-Ahmadi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
  @IBOutlet weak var imagesList: UITableView!
  
  var photos : [PhotoEntity] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    imagesList.dataSource = self
    imagesList.delegate = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    do {
      try photos = context.fetch(PhotoEntity.fetchRequest())
      imagesList.reloadData()
    } catch  {
      
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return photos.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    let photo = photos[indexPath.row]
    cell.textLabel?.text = photo.title
    cell.imageView?.image = UIImage(data: photo.image as! Data) // adds the image next to its title in the main table view
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let photo = photos[indexPath.row]
    performSegue(withIdentifier: "photoSegue", sender: photo)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let nextVC = segue.destination as! PhotoViewController
    nextVC.viewPicker = sender as? PhotoEntity
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
}

